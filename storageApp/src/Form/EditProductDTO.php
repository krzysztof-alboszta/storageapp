<?php

namespace App\Form;


class EditProductDTO
{
    private $productId;
    private $productName;
    private $newAmount;

    public function __construct(int $productId, string $productName, int $initialAmount)
    {
        $this->productId = $productId;
        $this->productName = $productName;
        $this->newAmount = $initialAmount;
    }

    public function getProductId(): int
    {
        return $this->productId;
    }

    public function getProductName(): string
    {
        return $this->productName;
    }

    public function setProductName(string $productName): void
    {
        $this->productName = $productName;
    }

    public function getNewAmount(): int
    {
        return $this->newAmount;
    }

    public function setNewAmount(int $newAmount): void
    {
        $this->newAmount = $newAmount;
    }
}