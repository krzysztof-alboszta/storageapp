<?php


namespace App\Controller;

use App\Controller\DTO\DemoParameters;
use App\Form\EditProductDTO;
use App\Form\EditProductType;
use App\Form\NewProductDTO;
use App\Form\NewProductType;
use Skladiste\SDKBundle\StorageSDK\API\Product as ProductAPI;
use Skladiste\SDKBundle\StorageSDK\SDKClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/demo")
 */
class DemoController extends AbstractController
{

    /**
     * @Route("/", name="storage_app_demo")
     */
    public function index(Request $request, SDKClientInterface $client): Response
    {
        /** @var ProductAPI $api */
        $api = $client->api('products');

        /** @var FormInterface $form */
        $form = $this->createForm(NewProductType::class, new NewProductDTO());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var NewProductDTO $newProduct */
            $newProduct = $form->getData();
            $response = $api->create([
                'product' => [
                    'name' => $newProduct->getProductName(),
                    'amount' => $newProduct->getInitialAmount(),
                ]
            ]);

            return $this->redirect($this->generateUrl('storage_app_demo'));
        }


        return $this->render('demo/index.html.twig', [
            'dto' => new DemoParameters(
                $api->all(),
                $api->available(),
                $api->unavailable(),
                $api->safeAvailable()
            ),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/update/{id}", name="storage_app_update_product")
     */
    public function update(int $id, Request $request, SDKClientInterface $client): Response
    {
        /** @var ProductAPI $api */
        $api = $client->api('products');
        $product = $api->show($id);
        $editProductDTO = new EditProductDTO($id, $product['name'], $product['amount']);
        $form = $this->createForm(EditProductType::class, $editProductDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var EditProductDTO $data */
            $data= $form->getData();
            $response = $api->update($id, [
                'product' => [
                    'name' => $data->getProductName(),
                    'amount' => $data->getNewAmount(),
                ]
            ]);

            return $this->redirect($this->generateUrl('storage_app_demo'));
        }


        return $this->render('demo/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
