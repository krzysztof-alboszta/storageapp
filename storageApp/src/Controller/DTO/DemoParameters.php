<?php


namespace App\Controller\DTO;

use Skladiste\RestBundle\Model\Product;

/**
 * helper object to enable proper typehinting in twig templates
 */
class DemoParameters
{
    private $allProducts = [];
    private $availableProducts = [];
    private $unAvailableProducts = [];
    private $safeAvailableProducts = [];

    /**
     */
    public function __construct(array $allProducts, array $availableProducts, array $unAvailableProducts, array $safeAvailableProducts)
    {
        $this->allProducts = $allProducts;
        $this->availableProducts = $availableProducts;
        $this->unAvailableProducts = $unAvailableProducts;
        $this->safeAvailableProducts = $safeAvailableProducts;
    }

    /** @return Product[] */
    public function getAllProducts(): array
    {
        return $this->allProducts;
    }

    /** @return Product[] */
    public function getAvailableProducts(): array
    {
        return $this->availableProducts;
    }

    /** @return Product[] */
    public function getUnAvailableProducts(): array
    {
        return $this->unAvailableProducts;
    }

    /** @return Product[] */
    public function getSafeAvailableProducts(): array
    {
        return $this->safeAvailableProducts;
    }


}