## Demo applications


### storageServer

Application responsible for storing products information in database and exposing access to them by API using package `skladiste/skladiste-rest-bundle`

#### configuration

exposed REST routes are configured by importing routing configuration file from the `SkladisteRestBundle` in `routes.yaml`:
```
skladiste_rest:
    resource: "@SkladisteRestBundle/Resources/config/routing/all.xml"
```

##### Mapping

`SkladisteRestBundle` provides abstract mapping for `Product` model, to define table for storing product the entity that extends bundle modle is defined 
with table name and primary key definitions

##### Persistence layer
configure DB parameters in `.env` file with  `DATABASE_URL` variable
next initialize database and schema with doctrine commands
```
cd storageServer
bin/console doctrine:database:create
bin/console doctrine:schema:create
```

#### Run

on local environment run the server demo application by
```bash
cd storageServer
bin/console server:run
```
this will run a webserver with bounded port. By default the port is 8000


### storageApp
Application uses package `skladiste/skladiste-sdk-client` as an SDK to connect with API exposed by `storageServer` application

to run properly application needs to have API url configured as environment variable `SKLADISTE_API_URI` 
or in `.env` file

```bash
SKLADISTE_API_URI=http://127.0.0.1:8000
```

after that client demo application can be run with
```bash
cd storageApp
bin/console server:run
```
and assuming that it will bind the port 8001 it can accessed by URL `http://localhost:8001/demo`