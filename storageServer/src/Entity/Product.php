<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Skladiste\RestBundle\Model\Product as ProductModel;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class Product extends ProductModel
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

}